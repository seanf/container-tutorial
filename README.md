WIN Containers educational session
==============================


This session is designed to introduce you to `docker` and `singularity`.


The session will be a mix of presentation and demo.  For the 
demo you will be able to follow along with a jupyter notebook on your own machine if
you wish.  To do so you will need to install ```docker```.  This is not an 
insignicant install, therefore it is OK to just watch the demo if you would prefer. 


If you wish to follow along on your own machine then you will need to do these steps:

1. Install docker from here:

     https://www.docker.com/products/docker-desktop

2. Clone this git repository on your laptop:

   ```bash
   git clone git@git.fmrib.ox.ac.uk:seanf/container-tutorial.git
   cd container-tutorial
   ```

3. Activate the fslpython environment (you need to have FSL 5.0.10 or newer installed to run this.):

   ```bash
   $FSLDIR/fslpython/bin/conda create python=3.6 jupyter notebook -p ./myenv
   source $FSLDIR/fslpython/bin/activate ./myenv
   pip install neurodocker

   ```


4. Start the notebook. This will open the notebook in a web browser tab:

   ```bash
   jupyter notebook containers.ipynb
   ```


You're ready to go!
